## Remerciements

Merci à tous ceux qui ont courageusement accepté d’être mentionnés dans cet ouvrage, ainsi qu’à ceux dont les réponses honnêtes et réfléchies m’ont aidée à affiner ma pensée pendant la phase de recherche :
André Arko, Brian Behlendorf, Adam Benayoun, Juan Benet, Cory Benfield, Kris Borchers, John Edgar, Maciej Fijalkowski, Karl Fogel, Brian Ford, Sue Graves, Eric Holscher, Brandon Keepers, Russell Keith-Magee, Kyle Kemp, Jan Lehnardt, Jessica Lord, Steve Marquess, Karissa McKelvey, Heather Meeker, Philip Neustrom, Max Ogden, Arash Payan, Stormy Peters, Andrey Petrov, Peter Rabbitson, Mikeal Rogers, Hynek Schlawack, Boaz Sender, Quinn Slack, Chris Soghoian, Charlotte Spencer, Harlan Stenn, Diane Tate, Max Veytsman, Christopher Allan Webber, Chad Whitacre, Meredith Whittaker, Doug Wilson. 
    
Merci à tous ceux qui ont écrit quelque chose de public qui a été référencé dans cet essai. C’était une partie importante de la recherche, et je remercie ceux dont les idées sont publiques pour que d’autres s’en inspirent.

Merci à Franz Nicolay pour la relecture et Brave UX pour le design de ce rapport.

Enfin, un très grand merci à Jenny Toomey et Michael Brennan pour m’avoir aidée à conduire ce projet avec patience et enthousiasme, à Lori McGlinchey et Freedman Consulting pour leurs retours et à Ethan Zuckerman pour que la magie opère.

Framasoft remercie chaleureusement les 40 traducteurs et traductrices du groupe Framalang qui depuis septembre ont contribué à vous proposer cet ouvrage (qui sera disponible en Framabook… quand il sera prêt) :

## Traduction

Framasoft et les éditions Framabook remercient chaleureusement les traducteurs et traductrices du groupe Framalang :

Adélie, AFS, alien spoon, Anthony, Asta (Gatien Bovyn), astraia_spica, Bam92 (Abel Mbula), Bidouille, Bromind (Martin Vassor), Ced, dominix, Edgar Lori, flo, glissière de sécurité, goofy, goudron, Julien / Sphinx, jums, Laure, Luc, Lumibd, lyn, Mika, MO, Opsylac (Diane Ranville), pasquin, Penguin, peupleLà, Piup, roptat, Rozmador, salade, serici, teromene, Théo, urlgaga, woof, xi (Juliette Tibayrenc), xXx


