## Une esquisse du tableau

Il est trop tôt pour dire à quoi devrait ressembler le soutien
institutionnel à long terme d’un point de vue prospectif, mais il y a
plusieurs domaines de travail critiques qui peuvent nous aider à le
déterminer. Les propositions suivantes sont rattachées à trois
domaines&nbsp;:

* *Traiter les infrastructures numériques comme un bien commun essentiel* et les élever au rang d’acteur intersectoriel clé&nbsp;;
* *Travailler avec des projets* pour améliorer les standards, la sécurité et les flux de production&nbsp;;
* *Augmenter la taille du groupe de contributeurs* de manière à ce que davantage de personnes, et davantage de personnes de types différents, puissent élaborer et soutenir ensemble les logiciels publics.

### Conscientiser et éduquer les acteurs clés

Comme nous l’avons relevé dans ce rapport, beaucoup d’acteurs clés —&nbsp;dont les *startups*, les gouvernements, et les sociétés de capital
risque&nbsp;— pensent à tort que les logiciels publics «&nbsp;fonctionnent, tout simplement&nbsp;» et ne requiert pas de maintenance supplémentaire.
Pour entretenir correctement l’écosystème de nos infrastructures
numériques, ces populations devraient être les premières à être
informées du problème. Les infrastructures numériques ont besoin de
porte-paroles qui soient affranchis de toute contrainte politique ou
commerciale et qui puissent comprendre et communiquer les besoins de
l’écosystème.

Traiter les infrastructures numériques comme des biens publics
essentiels pourrait également motiver l’investissement direct dans la
construction de meilleurs systèmes à partir de zéro. Par exemple, aux
États-Unis, les autoroutes inter-états et le réseau de bibliothèques
publiques furent dès l’origine conçus comme des ressources publiques.
Les unes et les autres ont eu leur champion (respectivement le Président
Dwight Eisenhower et le philanthrope Andrew Carnegie) qui ont clairement
argumenté en faveur du bénéfice social et financier qui résulterait de
tels projets.

Un réseau national d’autoroutes ne sert pas uniquement à nous relier en
tant qu’individus en facilitant les déplacements d’un endroit à un
autre, mais il apporte aussi la prospérité financière dans tous les
coins du pays, grâce à l’usage commercial des voies rapides pour
acheminer les marchandises. Dans les bibliothèques Andrew Carnegie,
publiques et gratuites, les livres étaient accessibles et non stockés en
magasin, pour permettre aux gens de les feuilleter et d’y trouver
eux-mêmes l’information sans avoir recours à un⋅e bibliothécaire. Cette
pratique a aidé à démocratiser l’information et à donner aux gens les
moyens de s’éduquer eux-mêmes.

Une meilleure éducation et une meilleure prise de conscience pourraient
s’étendre jusqu’aux gouvernements, dont certains ont rendu, par la loi,
les infrastructures numériques difficiles à soutenir et qui ne sont
peut-être pas familiers des normes culturelles et de l’histoire de
l’*open source*. Aux USA, l’IRS (Internal Revenue Service, service des impôts des États-Unis) a une définition très restrictive des activités caritatives, et comme l’*open source* est mal comprise, son
impact positif sur la société demeure ignoré[^P6ch2_a]. Cela complique
l’institutionnalisation de plus gros projets à travers une fondation ou
une association professionnelle.

### Mesurer l’utilisation et l’impact de l’infrastructure numérique

L’impact de l’infrastructure numérique est encore très difficile à
mesurer. Les indicateurs habituels sont soit très imprécis, soit
simplement indisponibles. Ce n’est pas un problème facile à résoudre.
Mais sans données relatives aux outils utilisés et à notre dépendance
vis-à-vis d’eux, il est difficile de se faire une idée nette de ce qui
manque de financement.

Avec de meilleurs indicateurs, nous pourrions décrire l’impact
économique de l’infrastructure numérique, identifier les projets
essentiels qui manquent de financement, et comprendre les dépendances
entre les projets et entre les personnes. Pour le moment, il est
impossible de dire qui utilise un projet *open source* à moins que
l’utilisateur, individu ou entreprise, ne le révèle. Pour déterminer
quel projet a besoin de plus de soutien, nous ne disposons que
d’informations anecdotiques.

De meilleures statistiques pourraient également nous aider à identifier
les «&nbsp;contributeurs clé de voûte&nbsp;». En biologie
environnementale, une «&nbsp;espèce clé&nbsp;» est une espèce animale qui
a un impact disproportionné sur son environnement au regard de ses
effectifs[^P6ch2_b]. Dans la même idée, un «&nbsp;contributeur clé&nbsp;» pourrait
être un développeur qui contribue à plusieurs projets essentiels, qui
est le seul responsable d’un projet crucial, ou qui est généralement
perçu comme influent et digne de confiance. Les «&nbsp;contributeurs
clés&nbsp;» sont des défenseurs essentiels, les valoriser en leur
fournissant les ressources dont ils ont besoin pourrait améliorer le
système dans son ensemble. Comprendre les relations entre les
communautés*open source* et les «&nbsp;contributeurs clés&nbsp;» pourrait
aider à identifier rapidement les secteurs qui auront besoin de soutien
supplémentaire.

On dispose également de peu de données sur les contributeurs
eux-mêmes&nbsp;: qui contribue à l’*open source*, quelles conditions leur
permettent de le faire, et quelles sont les contributions effectuées.
Les femmes, les non-anglophones, et les nouveaux contributeurs à l’*open
source* sont des exemples de population qui devraient être suivies dans
le temps, en particulier pour mesurer l’impact des programmes de
soutien.

Les seules statistiques disponibles sur les dépôts GitHub sont le nombre
de personnes ayant *étoilé* (action semblable à *liker*), vu
(c’est-à-dire qu’elles reçoivent des nouvelles du projet) ou
«&nbsp;forké&nbsp;» un projet. Ces chiffres permettent de fournir des
indicateurs concernant la popularité, mais ils peuvent être trompeurs.
Beaucoup de personnes peuvent étoiler un projet, parce qu’il a une
conception intéressante par exemple, sans toutefois l’intégrer à leur
propre code.

Certains gestionnaires de paquets tels npm (qui est celui de Node.js)
suivent les téléchargements. Le «&nbsp;*popularity contest*&nbsp;» de
Debian piste les téléchargements du système d’exploitation libre Debian.
Néanmoins, chaque gestionnaire de paquets est limité à un écosystème
particulier, et aucun de ces gestionnaires ne peut donner une image du
système dans son ensemble. Plusieurs projets ne sont pas inclus dans un
gestionnaire de paquets et ne sont pas suivis. Libraries.io, un site web
créé par Andrew Nesbitt, est une tentative pour agréger des données des
projets *open source* en fonction de leur usage, il piste environ 1,3
millions de bibliothèques *open source* sur 32 gestionnaires de paquets[^P6ch2_c].

### Travailler avec les projets pour moderniser l’organisation de travail

Beaucoup de projets sont en difficulté et pas seulement à cause d’un
manque de financement, mais aussi parce qu’il est difficile d’y
contribuer, ou encore parce qu’il existe un goulot d’étranglement au
niveau des mainteneurs qui traitent les demandes de modification (*pull
requests*) de la communauté. C’est vrai, en particulier, pour les plus
anciens projets qui ont été bâtis avec des outils de développement, des
langages et des processus qui ne sont plus populaires (ceux qui par
exemple utilisent un système de contrôle de version autre que Git, dont
la popularité croît chez les développeurs).

On peut faire beaucoup de choses pour faciliter la contribution à un
projet, depuis la migration vers un flux de travail (*workflow*) plus
moderne, le nettoyage du code, la fermeture des *pull request*
délaissées, jusqu’à la mise en place d’une politique claire pour les
contributions. Certains projets expérimentent pour rendre les
contributions plus simples. Le développeur Felix Geisendörfer, par
exemple, a suggéré que chaque personne qui soumet une modification du
code devrait avoir une permission de *commit* afin de réduire
l’engorgement au niveau de l’unique mainteneur vérifiant et approuvant
ces changements. Felix a estimé[^P6ch2_d] que «&nbsp;cette approche est un
fantastique moyen d’éviter que le projet ne se ratatine en transformant
le projet d’un seul homme en celui d’une communauté.&nbsp;»

Le règlement de contribution de Node.js, qui peut être adopté par les
autres projets Node, met l’accent sur l’augmentation du nombre de
contributeurs et sur leur autonomisation dans la prise de décision,
plutôt que de désigner les mainteneurs comme seule autorité
approbatrice. Leurs règles de contribution expliquent comment soumettre
et valider des *pull requests*, comment consigner des bugs, etc. Les
mainteneurs Node.js ont constaté qu’adopter de meilleures règles les
avait aidés à gérer leur charge de travail et à faire évoluer leur
communauté vers un projet plus sain et actif[^P6ch2_e]. 
Dans un premier temps, il y a des recherches à faire pour déterminer
quels projets doivent avancer. Autrement dit, à quoi ressemble un
«&nbsp;projet à succès&nbsp;», aussi bien en termes de financement et de
modèles de gouvernance, que dans l’équilibre à trouver entre
mainteneurs, contributeurs et usagers&nbsp;! La réponse peut varier en
fonction des différents types de projets et de leur ampleur.

### Encourager les standards communs dans les projets *open source*

Bien que GitHub soit en train de devenir une plateforme standard pour la
collaboration sur le code, de nombreux aspects des projets *open source*
ne sont pas encore standardisés, notamment l’ampleur et la richesse de
la documentation, des licences et des guides de contribution, ainsi que
le style de code et le formatage.

Encourager l’adoption de standards de projets pourrait faciliter, pour
les mainteneurs, la gestion des contributions, tout en réduisant pour
les contributeurs les obstacles à la participation.

Parmi les exemples de standardisation croissante, on trouve le code de
conduite, qui est un règlement détaillant les attentes en termes
d’attitude et de communication.

Ces dernières années, des codes de conduite ont été adoptés par un
nombre croissant de communautés de projets, notamment Node.js, Django et
Ruby. Bien que le processus d’adoption ait pu donner lieu à d’intenses
débats au sein de certaines communautés, leur prolifération révèle un
intérêt croissant pour la responsabilisation du comportement des
communautés.

### Augmenter le nombre de contributeurs et contributrices *open source*

Comme nous l’avons évoqué dans un chapitre précédent de ce rapport,
l’industrie du logiciel est florissante, avec un nombre croissant de
nouveaux développeurs mais aussi d’autres talents variés&nbsp;: il y a du
travail à faire pour encourager ces nouveaux arrivants à contribuer à
l’*open source*. Augmenter le nombre de contributeurs permet aux projets
*open source* d’être plus durables, car davantage de personnes
participent à leur développement. Permettre à davantage de personnes de
contribuer à l’*open source* accroît également l’empathie et la
communication entre les «&nbsp;utilisateurs&nbsp;» de l’*open source* et
les projets dont ils dépendent.

*Your First PR*[^P6ch2_f] («&nbsp;votre première PR&nbsp;», PR pour Pull
Request, NdT) est un exemple d’initiative, développée par la
programmeuse Charlotte Spencer, qui aide les nouveaux venus à effectuer
leur première contribution à l’*open source*. *First Timers
Only[^P6ch2_g]* (Réservé aux débutants) et *Make a Pull Request[^P6ch2_h]* 
(Faites une *pull request*) sont deux autres exemples de ressources
populaires qui introduisent les nouveaux venus à l’*open source*.
Certains projets *open source* utilisent également des étiquettes telles
que *first bug* ou *contributor friendly* pour
signaler les problèmes susceptibles d’être résolus par des contributeurs
moins expérimentés. Il serait également bénéfique d’encourager les
contributions à l’*open source* autres que le code, comme la rédaction
de documentation technique, la gestion des tâches et des flux de
travail, ou la création d’un site internet pour le projet.

En plus de l’augmentation de la proportion de techniciens talentueux
contribuant à l’*open source* existe la possibilité de puiser dans un
groupe de contributeurs plus large. Faire en sorte que les
non-anglophones se sentent bienvenus dans les communautés *open source*,
par exemple, pourrait rendre la technologie plus accessible à travers le
monde. Et comme beaucoup de recruteurs utilisent les travaux *open
source* comme un portfolio au moment d’embaucher un développeur, une
communauté *open source* plus diverse encouragerait l’apparition d’un
personnel technique globalement plus inclusif.

### Améliorer les relations entre projets et acteurs extérieurs

Les entreprises sont une pièce incontournable de l’écosystème *open
source*, et leur rôle ne fait que gagner en importance à mesure que
davantage d’entre elles adoptent les logiciels *open source*. Faciliter
la collaboration entre entreprises et projets, ainsi qu’aider les
entreprises à comprendre les besoins des communautés *open source*,
pourrait débloquer le soutien des entreprises susceptibles de devenir
mécènes ou promoteurs de l’*open source*.

Selon l’étude annuelle des entreprises *open source* réalisée par Black
Duck[^P6ch2_i], seulement 27% des entreprises ont un règlement formel
concernant les contributions de leurs employés à l’*open source*.
Clarifier la possibilité ou non pour les employés de contribuer à
l’*open source* sur leur temps de travail, et les encourager à le faire,
pourrait grandement améliorer le soutien des entreprises aux projets
*open source*.

En 2014, un groupement d’entreprises a fondé le TODO Group[^P6ch2_j], pour
partager les bonnes pratiques autour de la participation corporative à
l’*open source*. Parmi les membres de ce groupe, on trouve Box,
Facebook, Dropbox, Twitter et Stripe. En mars 2016, le TODO Group a
annoncé qu’il serait hébergé par la Fondation Linux en tant que projet
collaboratif[^P6ch2_k].

Les entreprises peuvent également fournir un soutien financier aux
projets, mais il est parfois difficile pour elles de trouver comment
formaliser leur mécénat. Créer des budgets dédiés au mécénat en
direction des équipes d’ingénieurs ou des employés, ou encore créer des
documents permettant aux projets de «&nbsp;facturer&nbsp;» plus facilement
leurs services aux entreprises, sont autant d’initiatives qui pourraient
augmenter les contributions financières à l’*open source*.

Poul-Henning Kamp, par exemple, travaille sur un projet *open source*
nommé Varnish[^P6ch2_l], utilisé par un dixième des sites les plus visités
d’internet, notamment Facebook, Twitter, Tumblr, The New York Times et
The Guardian. Pour financer ce travail, il a créé la Varnish Moral
License pour faciliter la sponsorisation du projet par les entreprises.

Même si en pratique la relation est un mécénat, Poul Henning utilise une
terminologie familière aux entreprises, avec des termes tels que
«&nbsp;facturation&nbsp;» et «&nbsp;licences&nbsp;», pour réduire les
obstacles à la participation[^P6ch2_m].

### Augmenter le soutien aux compétences diverses et aux fonctions hors-codage

Dans un passé pas si lointain, les *startups* de logiciels étaient
fortement centrées sur les compétences techniques. Les autres rôles,
comme le marketing et le design, étaient considérés comme secondaires
par rapport au code. Aujourd’hui, avec la création et la consommation
rapide de logiciels, cette conception ne tient plus. Les *startups* sont
en concurrence pour capter l’attention de leurs clients. L’identité de
la marque est devenue l’un des principaux facteurs de différenciation.

Ces cinq dernières années ont été celles de l’essor du développeur *full
stack* (polyvalent)&nbsp;: des développeurs plus généralistes que
spécialisés, capables de travailler sur plusieurs domaines d’un logiciel
complexe, et qui sont susceptibles d’avoir des compétences dans la
conception et la production. Les équipes de développement sont plus
soudées, elles utilisent les méthodes agiles avec des approches de
conception d’architecture logicielle (où le livrable est élaboré en
faisant des navettes entre les équipes de techniciens, designers et
commerciaux), plutôt qu’une approche en cascade (où chaque équipe
apporte sa pièce à l’édifice avant de la transmettre au groupe de
travail suivant).

Les projets *open source* ont connu peu d’évolutions de ce genre, malgré
notre dépendance croissante à leurs logiciels. On comprend aisément que
le code soit au cœur d’un projet *open source*, il est en quelque sorte
le «&nbsp;produit final&nbsp;» ou le livrable. Les fonctions relatives à
la gestion de la communauté, à la documentation, ou à la promotion du
projet, qui sont la marque d’une organisation saine et durable, sont
moins valorisées. Il en découle que les projets sont déséquilibrés.
Beaucoup de choses pourraient être entreprises pour financer et soutenir
les contributions autres que le code, des dons en nature pour payer les
serveurs par exemple, ou des avantages comme une assurance maladie.
Disposer de soutiens de ce type permettrait de réduire notablement la
charge des développeurs.

[^P6ch2_a]: Voir Partie 5, Chapitre 4
[^P6ch2_b]: Pour en savoir plus sur les espèces-clé, voir l'article [Espèce clé de voûte](https://fr.wikipedia.org/wiki/Espèce_clé_de_voûte) sur Wikipédia.
[^P6ch2_c]: Ces chiffres relevés sur le site [Libraries.io](https://libraries.io/), sont constamment mis à jour. En Janvier 2017 on compte environ 2,1 millions de bibliothèques *open source* recensées sur 33 gestionnaires de paquets.
[^P6ch2_d]: Voir Felix Geisendörfer, «&nbsp;[The Pull Request Hack](http://felixge.de/2013/03/11/the-pull-request-hack.html)&nbsp;», sur *felixge.de* (site personnel), 11/03/2013.
[^P6ch2_e]: Voir Mikeal, «&nbsp;[Healthy Open Source](https://medium.com/the-javascript-collection/healthy-open-source-967fa8be7951#.9uzyicpc6)&nbsp;», *Medium.com*, 22/02/2016.
[^P6ch2_f]: Voir le compte Twitter [Your First PR@yourfirstpr](https://twitter.com/yourfirstpr).
[^P6ch2_g]: Voir [Firsttimersonly.com](http://www.firsttimersonly.com/).
[^P6ch2_h]: Voir [Makeapullrequest.com](http://makeapullrequest.com/).
[^P6ch2_i]: Voir le rapport de la société BlackDuck *[The Ninth Annual Future of Open Source Survey](https://www.blackducksoftware.com/2015-future-of-open-source)*, sur blackducksoftware.com, 2015.
[^P6ch2_j]: Voir [Todogroup.org](http://todogroup.org/).
[^P6ch2_k]: Voir TheTodoGroup, «&nbsp;[TODO Becomes A Linux Foundation Collaborative Project](http://todogroup.org/blog/todo-becomes-lf-collaborative-project/)&nbsp;», sur le site *Todo.org*, 30/03/2016.
[^P6ch2_l]: Pour en savoir plus sur Varnish, voir l'article «&nbsp;[Varnish](https://fr.wikipedia.org/wiki/Varnish)&nbsp;» sur Wikipédia.
[^P6ch2_m]: Voir Poul-Henning Kamp, «&nbsp;[The Varnish Moral License](http://phk.freebsd.dk/VML/)&nbsp;», sur la page personnelle de l'auteur, 30/06/2015.


