## Élaborer des stratégies d’assistance efficaces

Même si les gens sont de plus en plus intéressés par les efforts pour
soutenir les infrastructures numériques, les initiatives actuelles sont
encore récentes, faites pour des cas particuliers ou fournissent
seulement un support partiel (comme le partage d’avantages fiscaux par
des organisations à but non lucratif avec des groupes extérieurs à
celles-ci).

Le développement de stratégies de soutien efficaces demande une
compréhension fine de la culture *open source* qui caractérise une très
grande partie de notre infrastructure numérique, mais aussi de
reconnaître que beaucoup de choses ont changé dans les cinq dernières
années, y compris la définition même de l’*open source*.

L’argent seul ne suffira pas à répondre aux problèmes d’un projet
d’infrastructure en difficulté, parce que l’*open source* s’épanouit
grâce aux ressources humaines et non financières. Il existe beaucoup de
façons d’accroître les ressources humaines, comme distribuer la charge
de travail parmi davantage de contributeurs ou encourager les
entreprises à faire publier en *open source* une partie du travail de
leurs employés. Une stratégie de soutien efficace doit inclure plusieurs
façons de générer du temps et des ressources au-delà du financement
direct du développement. Elle doit partir du principe que l’approche
*open source* n’est pas défectueuse en elle-même, mais manque simplement
de ressources.

Soutenir les infrastructures nécessite d’intégrer le concept
d’intendance en lieu et place du concept de contrôle. Comme nous l’avons
vu, les infrastructures numériques ne ressemblent pas aux
infrastructures physiques. Elles sont réparties entre de multiples
acteurs et organisations, avec des projets de toute forme et de toute
taille, et il est difficile de prédire quels projets deviendront un
succès ou qui y contribuera sur le long terme.

Avec cela en tête, voici quelques clés pour élaborer une stratégie
d’assistance efficace&nbsp;:

Adopter la décentralisation, plutôt que s’y opposer

: Les ressources de l’*open source* sont destinées à être partagées, c’est
en partie ce qui leur donne autant d’impact. Utiliser la force que donne
l’aspect communautaire comme un levier, plutôt que de recentraliser
l’autorité.

Travailler étroitement avec les communautés informatiques existantes

: Les communautés informatiques sont actives, soudées et savent se faire
entendre. Faites appel à elles plutôt que de prendre une décision en
aparté. Les voix les plus sonores des communautés agissent comme un
signal de danger quand un problème nécessite d’être soulevé.

Envisager une approche globale du soutien aux projets

: Les projets ont besoin de bien plus que du code ou de l’argent, parfois
même ils n’ont besoin ni de l’un ni de l’autre. Le soutien sur le long
terme est davantage une question de temps accordé que d’argent. La revue
de code, la documentation technique, les tests de code, la soutien de la
communauté, et la promotion du projet constituent un ensemble de
ressources importantes.

Aider les mainteneurs de projets à anticiper

: Aujourd’hui, les efforts pour soutenir l’infrastructure numérique ont
tendance a être uniquement de la réactivité liée aux circonstances
ponctuelles. En plus des projets existants, il existe sûrement de
nouveaux projets qui ont besoin d’être lancés et accompagnés. Pour les
projets existants, les mainteneurs trouveront un grand avantage à
pouvoir planifier en vue des trois à cinq ans à venir, et pas seulement
pour six mois ou un an.

Voir les opportunités, pas seulement les risques

: Soutenir l’*open source* de nos jours, cela ne consiste pas uniquement à
éviter les scénarios catastrophes (par exemple les failles de sécurité),
mais plutôt à donner les moyens à davantage de personnes de réaliser
davantage de choses. Ce concept est une caractéristique essentielle de
la culture *open source* actuelle, et permet aussi de mettre en place un
soutien pérenne. Tenez compte dans votre stratégie de la façon dont vous
pourriez accueillir davantage de personnes d’horizons, de compétences et
de talents différents, plutôt que de limiter l’activité pour favoriser
les personnes qui participent déjà.

David Heinemeier Hansson, le créateur de Ruby on Rails, compare l’*open
source* à un récif de corail[^P6ch1_a]&nbsp;:

> C’est un milieu plus fragile que vous ne le pensez, et il est
> difficile de sous-estimer la beauté qui est involontairement en jeu.
> Marchez avec précaution.

[^P6ch1_a]: David Heinemeier Hansson, «&nbsp;[The perils of mixing open source and money](http://david.heinemeierhansson.com/2013/the-perils-of-mixing-open-source-and-money.html)&nbsp;», *blog personnel*, 12/11/2013.


