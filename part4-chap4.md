## Les efforts institutionnels pour financer les infrastructures numériques

Il existe des institutions qui s’efforcent d’organiser collectivement
les projets *open source* et aider à leur financement. Il peut s’agir de
fondations indépendantes liées aux logiciels, ou d’entreprises de
logiciels elles-mêmes qui apportent leur soutien.

### Soutien administratif et mécénat financier

Plusieurs fondations fournissent un soutien organisationnel, comme le
mécénat financier, aux projets *open source*&nbsp;: en d’autres termes,
la prise en charge des tâches autres que le code, dont beaucoup de
développeurs se passent volontiers. 

L’Apache Software Foundation,
constituée en 1999, a été créée en partie pour soutenir le développement
du serveur Apache HTTP, qui dessert environ 55&nbsp;% de la totalité des
sites internet dans le monde[^P5ch4_a]. Depuis lors, la fondation Apache est devenue un foyer d’ancrage pour
plus de 350 projets[^P5ch4_b] *open source*. Elle se structure comme une
communauté décentralisée de développeurs, sans aucun employé à plein
temps et avec presque 3000 bénévoles. Elle propose de multiples services
aux projets qu’elle héberge, consistant principalement en un soutien
organisationnel, juridique et de développement. En 2011, Apache avait un
budget annuel de plus de 500&nbsp;000&nbsp;$, issu essentiellement de
subventions et de donations[^P5ch4_c].

Le Software Freedom Conservancy, fondée en 2006, fournit également des
services administratifs non-lucratifs à plus de 30&nbsp;projets libres et
*open source[^P5ch4_d]*. Parmi les projets que cette fondation soutient, on
retrouve notamment Git, le système de contrôle de versions dont nous
avons parlé plus haut et sur lequel GitHub a bâti sa plateforme, et
Twisted, une librairie Python déjà citée précédemment[^P5ch4_e].

On trouve encore d’autres fondations fournissant un soutien
organisationnel, par exemple The Eclipse Foundation et Software in the
Public Interest. La Fondation Linux et la Fondation Mozilla soutiennent
également des projets *open source* externes de diverses façons (dont
nous parlerons plus loin dans ce chapitre), bien que ce ne soit pas le
but principal de leur mission.

Il est important de noter que ces fondations fournissent une aide
juridique et administrative, mais rarement financière. Ainsi, être
sponsorisé par Apache ou par le Software Freedom Conservancy ne suffit
pas en soi à financer un projet&nbsp;; les fondations ne font que
faciliter le traitement des dons et la gestion du projet.

Un autre point important à noter, c’est que ces initiatives soutiennent
le logiciel libre et *open source* d’un point de vue philosophique, mais
ne se concentrent pas spécifiquement sur ses infrastructures. Par
exemple, OpenTripPlanner, projet soutenu par le Software Freedom
Conservancy, est un logiciel pour planifier les voyages&nbsp;: même si son
code est *open source*, il s’agit d’une application destinée aux
consommateurs, pas d’une infrastructure.

### Créer une fondation pour aider un projet

Certains projets sont suffisamment importants pour être gérés à travers
leurs propres fondations. Python, Node.js, Django et jQuery sont tous
adossés à des fondations.

Il y a deux conditions fondamentales à remplir pour qu’une fondation
fonctionne&nbsp;: accéder au statut d’exemption fiscale et trouver des
financements.

Réussir à accéder au statut 501(c3)[^P5ch4_f], la loi américaine qui
définit les organismes sans but lucratif, peut s’avérer difficile pour
ces projets, à cause du manque de sensibilisation autour de la
technologie *open source* et de la tendance à voir l’*open source* comme
une activité non-caritative. En 2013, une controverse a révélé que l’IRS
(Internal Revenue Service, service des impôts américain) avait dressé
une liste de groupes postulant au statut d’exemption fiscale qui
nécessiteraient davantage de surveillance&nbsp;: l’*open source* en
faisait partie[^P5ch4_g][^P5ch4_h]. Malheureusement, ces contraintes ne facilitent pas
l’institutionnalisation de ces projets.

Par exemple, Russell Keith-Magee, qui était jusqu’à une époque récente
président de la Django Software Foundation, a expliqué que la fondation
ne pouvait pas directement financer le développement logiciel de Django,
sans prendre le risque de perdre son statut 501(c3). La fondation
soutient plutôt le développement via des activités communautaires.

En juin 2014, la Fondation Yorba, qui a créé des logiciels de
productivité qui tournent sous Linux, s’est vu refuser le statut 501(c3)
après avoir attendu la décision pendant presque quatre ans et demi. Jim
Nelson, son directeur exécutif, a été particulièrement inquiété par le
raisonnement de l’IRS&nbsp;: parce que leur logiciel pouvait
potentiellement être utilisé par des entités commerciales, le travail de
Yorba ne pouvait pas être considéré comme caritatif. Une lettre de l’IRS
explique[^P5ch4_i]&nbsp;:

> Se contenter de publier sous une licence *open source* tous usages
> ne signifie pas que les pauvres et les moins privilégiés utiliseront
> effectivement les outils. (…) On ne peut pas savoir qui utilise les
> outils, et encore moins quel genre de contenus sont créés avec ces
> outils.

Nelson a pointé les failles de ce raisonnement dans un billet de blog,
comparant la situation à celle d’autres biens publics[^P5ch4_j]&nbsp;:

> Il y a une organisation caritative ici à San Francisco qui
> plante des arbres pour le bénéfice de tous. Si l’un de leurs arbres…
> rafraîchit les clients d’un café pendant qu’ils profitent de leur
> expresso, cela signifie-t-il que l’organisation qui plante des arbres
> n’est plus caritative&nbsp;?

Les projets qui accèdent au statut 501(c3) ont tendance à insister sur
l’importance de la communauté, comme la Python Software Foundation, dont
l’objet est[^P5ch4_k] de «&nbsp;promouvoir, protéger et faire progresser le langage
de programmation Python, ainsi que de soutenir et faciliter la
croissance d’une communauté diversifiée et internationale de
programmeurs Python.&nbsp;»

En parallèle, certains projets candidatent pour devenir une association
de commerce au sens du statut 501(c6)[^P5ch4_l]. La Fondation jQuery en est un exemple, se décrivant[^P5ch4_m] comme «&nbsp;une
association de commerce à but non-lucratif pour développeurs web,
financée par ses membres&nbsp;». La Fondation Linux est également une
association de commerce.

Le deuxième aspect de la formalisation de la gouvernance d’un projet à
travers une fondation est la recherche de la source de financement
adéquate. Certaines fondations sont financées par des donations
individuelles, mais ont proportionnellement de petits budgets.

La Django Software Foundation, par exemple, gère Django, le plus
populaire des *frameworks web* écrits en Python, utilisé par des
entreprises comme Instagram et Pinterest. La Fondation est dirigée par
des bénévoles, et reçoit moins de 60&nbsp;000&nbsp;$ de donations par an[^P5ch4_n].
L’année dernière, la Django Software Foundation a reçu une subvention
ponctuelle de la part de la Fondation Mozilla[^P5ch4_o].

Parmi les autres sources habituelles de financement on trouve les
entreprises mécènes. En effet, les entreprises privées sont bien placées
pour financer ces projets logiciels, puisqu’elles les utilisent
elles-mêmes. La Fondation Linux est l’un de ces cas particuliers qui
rencontrent le succès, et ce grâce la valeur fondamentale du noyau Linux
pour les activités de quasiment toutes les entreprises. La Fondation
Linux dispose de 30 millions de dollars d’un capital géré sur une base
annuelle, alimenté par des entreprises privées comme IBM, Intel, Oracle
et Samsung –&nbsp;et ce chiffre continue d’augmenter[^P5ch4_p].

Créer une fondation pour soutenir un projet est une bonne idée pour les
projets d’infrastructure très conséquents. Mais cette solution est moins
appropriée pour de plus petits projets, en raison de la quantité de
travail, des ressources, et du soutien constant des entreprises,
nécessaires pour créer une organisation durable.

Node.js est un exemple récent d’utilisation réussie d’une fondation pour
soutenir un gros projet. Node.js est un *framework* JavaScript,
développé en 2009 par Ryan Dahl et différents autres développeurs
employés par Joyent, une entreprise privée du secteur logiciel. Ce
*framework* est devenu extrêmement populaire, mais a commencé à souffrir
de contraintes de gouvernance liées à l’encadrement par Joyent, que
certaines personnes estimaient incapable de représenter pleinement la
communauté enthousiaste et en pleine croissance de Node.js.

En 2014, un groupe de contributeurs de Node.js menaça de *forker* le
projet. Joyent essaya de gérer ces problèmes de gouvernance en créant un
conseil d’administration pour le projet, mais la scission eut finalement
lieu, le nouveau *fork* prenant le nom d’io.js[^P5ch4_q]. En février 2015 fut
annoncée l’intention de créer une organisation 501(c)(6) en vue
d’extraire Node.js de la mainmise de Joyent. Les communautés Node.js et
io.js votèrent pour travailler ensemble sous l’égide de cette nouvelle
entité, appelée la Fondation Node.js. La Fondation Node.js, structurée
suivant les conseils de la Fondation Linux, dispose d’un certain nombre
d’entreprises mécènes qui contribuent financièrement à son budget,
notamment IBM, Microsoft et payPal[^P5ch4_r]. Ces sponsors pensent retirer une
certaine influence de leur soutien au développement d’un projet logiciel
populaire qui fait avancer le web, et ils ont des ressources à mettre à
disposition.

Un autre exemple prometteur est Ruby Together, une organisation initiée
par plusieurs développeurs Ruby pour soutenir des projets
d’infrastructure Ruby. Ruby Together est structurée en tant
qu’association commerciale, dans laquelle chaque donateur, entreprise ou
individu, investit de l’argent pour financer le travail à temps plein de
développeurs chargés d’améliorer le cœur de l’infrastructure Ruby. Les
donateurs élisent un comité de direction bénévole, qui aide à décider
chaque mois sur quels projets les membres de Ruby Together devraient
travailler.

Ruby Together fut conçue par deux développeurs et finance leur travail, il s'agit d'André Arko et de David Radcliffe. Aujourd’hui, en avril 2016,
est également rémunéré le travail de quatre autres mainteneurs
d’infrastructure. Le budget mensuel en mars 2016 était d’un peu plus de
18&nbsp;000 dollars par mois, couvert entièrement par des dons. La
création de Ruby Together fut annoncée en mars 2015 et reste un projet
récent, mais pourrait bien servir de base à un modèle davantage orienté
vers la communauté pour financer la création d’autres projets
d’infrastructure[^P5ch4_s].

### Programmes d’entreprises

Les éditeurs de logiciels soutiennent les projets d’infrastructure de
différentes manières.

En tant que bénéficiaires des projets d’infrastructures, ils contribuent
en faisant remonter des dysfonctionnements et des bugs, en proposant ou
soumettant de nouvelles fonctionnalités ou par d’autres moyens.
Certaines entreprises encouragent leurs employés à contribuer à des
projets d’une importance critique sur leur temps de travail. De nombreux
employés contribuent ainsi de manière significative à des projets *open
source* extérieurs à l’entreprise. Pour certains employés, travailler
sur de l’*open source* fait clairement partie de leur travail.
L’allocation de temps de travail de leurs salariés est une des plus
importantes façons de contribuer à l’*open source* pour les entreprises.

Les grandes entreprises comme Google ou Facebook adhèrent avec
enthousiasme à l’*open source*, de façon à inspirer confiance et
renforcer leur influence&nbsp;; elles sont de fait les seuls acteurs
institutionnels assez importants qui peuvent assumer son coût sans avoir
besoin d’un retour financier sur investissement. Les projets *open
source* aident à renforcer l’influence d’une entreprise, que ce soit en
publiant son propre projet *open source* ou en embauchant des
développeurs de premier plan pour qu’ils travaillent à plein temps sur
un projet *open source*.

Ces pratiques ne sont pas limitées aux entreprises purement logicielles.
Walmart, par exemple, qui est un soutien majeur de l’*open source*, a
investi plus de deux millions de dollars dans un projet *open source*
nommé hapi[^P5ch4_t]. Eran Hammer, développeur senior à Walmart Labs, s’est
empressé de préciser[^P5ch4_u] que «&nbsp;l’*open source*, ce n’est pas du
caritatif&nbsp;» et que les ressources d’ingénierie gratuites sont
proportionnelles à la taille des entreprises qui utilisent hapi. Dion
Almaer, l’ancien vice-président en ingénierie de Walmart Labs, a
remarqué que leur engagement envers l’*open source* les aidait à
recruter, à construire une solide culture d’entreprise, et à gagner
«&nbsp;une série d’effets de levier[^P5ch4_v]&nbsp;».

En termes de soutien direct au maintien du projet, il arrive que des
entreprises embauchent une personne pour travailler à plein temps à la
maintenance d’un projet *open source*. Les entreprises donnent aussi
occasionnellement à des campagnes de financement participatif pour un
projet particulier. Par exemple, récemment, une campagne sur Kickstarter
pour financer un travail essentiel sur Django a reçu 32&nbsp;650&nbsp;&#163;
(environ 40&nbsp;000&nbsp;&euro;)&nbsp;; Tom Christie, l’organisateur de la campagne, a
déclaré[^P5ch4_w] que 80&nbsp;% du total venait d’entreprises. Cependant, ces
efforts sont toujours consacrés à des projets&nbsp; spécifiques et les
infrastructures numériques ne sont pas encore vues communément comme une
question de responsabilité sociale par les entreprises de logiciel à but
lucratif. Cela laisse encore beaucoup de marge aux actions de défense et
promotion.

L’un des programmes d’entreprise les plus connus est le *Summer of Code*
de Google (été de programmation, souvent nommé GSoC), déjà mentionné
dans ce livre[^P5ch4_x], qui offre de l’argent à des étudiants pour travailler
sur des projets *open source* pendant un été. Les étudiants sont
associée·e·s à des mentors qui vont les aider à se familiariser avec le
projet. Le *Summer of Code* est maintenu par le bureau des programmes
*open source* de Google, et il a financé des milliers d’étudiants[^P5ch4_y][^P5ch4_z].

Le but du *Summer of Code* est de donner à des étudiants la possibilité
d’écrire du code pour des projets *open source*, non de financer les
projets eux-mêmes.

L’an dernier, Stripe, une entreprise de traitement des paiements, a
annoncé une «&nbsp;retraite&nbsp; *open source*&nbsp;», offrant un salaire
mensuel d’un maximum de 7500 dollars pour une session de trois mois dans
les locaux de Stripe[^P5ch4_aa]. À l’origine, l’entreprise voulait uniquement
offrir deux bourses, mais après avoir reçu 120 candidatures, le
programme a été ouvert à quatre bénéficiaires.

Ces derniers ont été enchantés par cette expérience. L’un d’entre eux,
Andrey Petrov, continue de maintenir la bibliothèque Python urllib3 dont
nous avons déjà parlé[^P5ch4_ab], et qui est largement utilisée dans l’écosystème
Python.

À propos de cette expérience, Andrey a écrit[^P5ch4_ac]&nbsp;:

> La publication et la contribution au code *open source* vont
> continuer que je sois payé pour ou non, mais le processus sera lent et
> non ciblé. Ce qui n’est pas un problème, car c’est ainsi que l’*open
> source* a toujours fonctionné. Mais on n’est pas obligé d’en rester là. (…)
>
> Si vous êtes une entreprise liée à la technologie, allouez s’il vous
> plaît un budget pour du financement et des bourses dans le domaine de
> l’open source. Distribuez-le sur Gittip (Gittip est maintenant dénommé
> Gratipay. Le produit a été quelque peu modifié depuis la publication
> originelle du billet d’Andrew) si vous voulez, ou faites ce qu’a fait
> Stripe et financez des sprints ambitieux pour atteindre des objectifs
> de haute valeur.
>
> Considérez ceci comme une demande solennelle de parrainage&nbsp;: s’il
> vous plaît, aidez au financement du développement d’urllib3.

La retraite *open source* de Stripe peut servir de modèle aux programmes
de soutien. Stripe a décidé de reconduire le programme pour une deuxième
année consécutive en 2015. Malgré la popularité de leur programme et la
chaude réception qu’il a reçue chez les développeurs et développeuses,
cette pratique n’est toujours pas répandue dans les autres entreprises.

Les entreprises montrent un intérêt croissant pour l’*open source*, et
personne ne peut prédire au juste ce que cela donnera sur le long terme.
Les entreprises pourraient régler le problème du manque de support à
long terme en consacrant des ressources humaines et un budget aux
projets *open source*. Des programmes de bourse formalisés pourraient
permettre de mettre en contact des entreprises avec des développeurs
*open source* ayant besoin d’un soutien à plein temps. Alors que les
équipes de contributeurs à un projet étaient souvent composées d’une
diversité de développeurs venant de partout, peut-être seront-elles
bientôt composées par un groupe d’employés d’une même entreprise. Les
infrastructures numériques deviendront peut-être une série de
«&nbsp;jardins clos&nbsp;», chacun d’entre eux étant techniquement ouvert
et bénéficiant d’un soutien solide, mais en réalité, grâce à ses
ressources illimitées, une seule entreprise et de ses employés en
assureront le soutien.

Mais si on pousse la logique jusqu’au bout, ce n’est pas de très bon
augure pour l’innovation. Jeff Lindsay, un architecte logiciel qui a
contribué à mettre en place l’équipe de Twilio, une entreprise 
performante de solutions de communication dans le *cloud*, livrait&nbsp;
l’an dernier ses réflexions dans une émission[^P5ch4_ad]&nbsp;:

> À Twilio, on est incité à améliorer le fonctionnement de Twilio,
> à Amazon on est incité à améliorer le fonctionnement d’Amazon. Mais
> qui est incité à mieux les faire fonctionner ensemble et à offrir plus
> de possibilités aux usagers en combinant les deux&nbsp;? Il n’y a
> personne qui soit vraiment incité à faire ça.

Timothy Fuzz, un ingénieur système, ajoute[^P5ch4_ae]&nbsp;:

> Pour Bruce Schneier, cette situation tient du servage. Nous
> vivons dans un monde où Google est une cité-état, où Apple est une
> cité-état et… si je me contente de continuer à utiliser les produits
> Google, si je reste confiné dans l’environnement Google, tout me
> paraît bénéfique. Mais il est quasi impossible de vivre dans un monde
> où je change d’environnement&nbsp;: c’est très pénible, vous tombez sur
> des bugs, et aucune de ces entreprises ne cherche vraiment à vous
> aider. Nous sommes dans ce monde bizarre, mais si vous regardez du
> côté des cités-états, l’un des problèmes majeurs c’est le commerce
> inter-étatique&nbsp;: si on doit payer des droits de douane parce qu’on
> cherche à exporter quelque chose d’Austin pour le vendre à Dallas, ce
> n’est pas un bon modèle économique. On pâtit de l’absence d’innovation
> et de partage des idées. On en est là, aujourd’hui.

Bien que l’argument du «&nbsp;servage&nbsp;» se réfère généralement aux
produits d’une entreprise, comme l’addiction à l’iPhone ou à Android, il
pourrait être tout aussi pertinent pour les projets *open source*
parrainés. Les améliorations prioritaires seront toujours celles qui
bénéficient directement à l’entreprise qui paie le développeur. Cette
remarque ne relève pas de la malveillance ou de la conspiration&nbsp;:
simplement, être payé par une entreprise pour travailler à un projet qui
ne fait pas directement partie de ses affaires est une contrainte à
prendre en compte.

Mais personne, pas plus Google que la Fondation Linux ou qu’un groupe de
développeurs indépendants, ne peut contrôler l’origine d’un bon projet
*open source*. Les nouveaux projets de valeur peuvent germer n’importe
où, et quand ils rendent un service de qualité aux autres développeurs,
ils sont largement adoptés. C’est une bonne chose et cela alimente
l’innovation.

### Aide spécifique de fondation

Deux fondations ont récemment fait part de leur décision de financer
plus spécifiquement l’infrastructure numérique&nbsp;: la Fondation Linux
et la Fondation Mozilla.

Après la découverte de la faille Heartbleed, la Fondation Linux a
annoncé qu’elle mettait en place l’Initiative pour les infrastructures
essentielles (Core Infrastructure Initiative, CII) pour éviter que ce
genre de problème ne se reproduise. Jim Zemlin, le directeur-général de
la Fondation Linux, a réuni près de 4&nbsp;millions de dollars en promesses
de dons provenant de treize entreprises privées, dont Amazon Web
Services, IBM et Microsoft, pour financer des projets liés à la sécurité
des infrastructures pour les trois ans à venir[^P5ch4_af]. La Fondation Linux
s’occupe également d’obtenir des financements gouvernementaux, y compris
de la Maison-Blanche[^P5ch4_ag].

La CII est officiellement un projet de la fondation Linux. Depuis sa
création en avril 2014, la CII a sponsorisé du travail de développement
d’un certain nombre de projets, dont OpenSSL, NTP, GnuPG (un système de
chiffrement des communications) et OpenSSH (un ensemble de protocoles
relatifs à la sécurité). La CII se concentre en priorité sur une partie
de l’infrastructure numérique&nbsp;: les projets relatifs à la sécurité.

Au mois d’octobre 2015, Mitchell Baker, la présidente de la Fondation
Mozilla, a annoncé la création du Programme de soutien à l’*open source*
de Mozilla (Mozilla Open Source Support Program, MOSS) et a promis de
consacrer un million de dollars au financement de logiciels libres et
*open source*. Selon Baker, ce programme aura deux volets&nbsp;: un volet
«&nbsp;rétribution&nbsp;» pour les projets qu’utilise Mozilla et un volet
«&nbsp;contribution&nbsp;» pour les projets libres et *open source* en
général. Grâce aux suggestions de la communauté, Mozilla a sélectionné
neuf projets pour la première série de bourses[^P5ch4_ah]. Ils se disent également
prêts à financer des audits de sécurité pour les projets *open source*
importants[^P5ch4_ai].

Enfin, certaines fondations contribuent ponctuellement à des projets de
développement logiciel. Par exemple, la Python Software Foundation
propose aux individus et aux associations des bourses modestes destinées
pour la plupart aux actions pédagogiques et de sensibilisation[^P5ch4_aj].

### Autres acteurs institutionnels

Il existe plusieurs autres acteurs qui apportent diverses formes de
soutien aux infrastructures numériques&nbsp;: Github, le capital-risque
et le monde universitaire. 

Si Facebook est un «&nbsp;utilitaire
social[^P5ch4_ak]&nbsp;» et Google un «&nbsp;utilitaire de recherche&nbsp;», tous deux
régulant *de facto* les corps dans leur domaine respectif – alors Github
a une chance de devenir «&nbsp;l’utilitaire *open source*&nbsp;». Son
modèle économique l’empêche de devenir un mastodonte financier
(contrairement à Facebook ou Google dont le modèle est basé sur la
publicité, alors que Github se monétise par l’hébergement de code pour
les clients professionnels, et par l’hébergement individuel de code
privé), mais Github est toujours un endroit où aujourd’hui encore
l’*open source* est créé et maintenu.

Github s’est doté de grandes aspirations avec une levée de fonds de
capital-risque de 350&nbsp;millions de dollars, même si l’entreprise était
déjà rentable. Si Github assume pleinement son rôle d’administrateur du
code *open source*, l’organisation peut avoir une énorme influence sur
le soutien apporté à ces projets. Par exemple, elle peut créer de
meilleurs outils de gestion de projets *open source*, défendre certaines
catégories de licences, ou aider les gestionnaires de projets à gérer
efficacement leurs communautés.

Github a subi de grosses pressions venant des développeurs qui gèrent
certains projets, ces pressions incluent une lettre ouverte
collective[^P5ch4_al] intitulée
«&nbsp;Cher Github&nbsp;», principalement issue de la communauté
JavaScript. Cette lettre explique&nbsp;: «&nbsp;Beaucoup sont frustrés.
Certains parmi nous qui déploient des projets très populaires sur Github
se sentent totalement ignorés par vous&nbsp;». La lettre inclut une liste
de requêtes pour l’amélioration de produits, qui pourrait les aider à
gérer plus efficacement leurs projets.

Github se confronte de plus en plus à des difficultés largement
documentées dans les médias. Auparavant, l’entreprise était connue pour
sa hiérarchie horizontale, sans aucun manager ni directive venant d’en
haut. Les employés de Github avaient aussi la liberté de choisir de
travailler sur les projets qu’ils souhaitaient[^P5ch3_am]. Ces dernières années,
tandis que Github s’est développé pour atteindre presque 500 employés,
l’entreprise a réorienté sa stratégie vers une orientation plus
commerciale en recrutant des équipes de vente et des dirigeants, insérés
dans un système hiérarchique plus traditionnel. Cette transition d’une
culture décentralisée vers plus de centralité s’est faite dans la
douleur chez Github&nbsp;: au moins 10&nbsp;dirigeants ont quitté
l’organisation durant les quelques mois de l’hiver 2015-2016, ces
départs incluant l’ingénieur en chef, le directeur des affaires
financières, le directeur stratégique et le directeur des ressources
humaines[^P5ch4_an]. En raison de ces conflits internes, Github n’a toujours pas
pris position publiquement pour jouer un rôle de promoteur de l’*open
source* et assumer un leadership à même de résoudre les questions
pressantes autour de l’*open source*, mais le potentiel est bel et bien
là.

Pour le capital-risque, abordé précédemment, il y a un enjeu particulier
dans l’avenir des infrastructures numériques. Comme les outils des
développeurs aident les entreprises du secteur technologique à créer
plus rapidement et plus efficacement, meilleurs sont les outils,
meilleures sont les *startups*, meilleure sera la rentabilité du
capital-risque. Néanmoins, l’infrastructure, d’un point de vue
capitaliste, n’est en rien limitée à l’*open source* mais plus largement
focalisée sur les plateformes qui aident d’autres personnes à créer.
C’est pour cela que les investissements dans Github ou npm, qui sont des
plateformes qui aident à diffuser du code source, ont un sens, mais tout
aussi bien les investissements dans Slack, une plateforme de travail
collaboratif que les développeurs peuvent utiliser pour construire des
applications en ligne de commande connectées à la plateforme (à ce
propos, le capital-risque a constitué un fonds de 80&nbsp;millions dédié au
support de projets de développement qui utilisent Slack[^P5ch4_ao]). Même si le
capital-risque apprécie les mécaniques sous-jacentes de
l’infrastructure, il est limité dans ses catégories d’actifs&nbsp;: un
capitaliste ne peut pas investir dans un projet sans modèle économique.

Enfin, les institutions universitaires ont joué un rôle historique
éminent dans le soutien aux infrastructures numériques, tout
particulièrement le développement de nouveaux projets. Par exemple,
LLVM, un projet de compilateur pour les langages C et C++, a démarré en
tant que projet de recherche au sein de l’Université de l’Illinois, à
Urbana-Champaign. Il est maintenant utilisé par les outils de
développement de Mac OS X et iOS d’Apple, mais aussi dans le kit de
développement de la Playstation&nbsp;4 de Sony.

Un autre exemple, R, un langage de programmation répandu dans la
statistique assistée par ordinateur et l’analyse de données, a été
d’abord écrit par Robert Gentleman et Ross Ihaka à l’Université
d’Auckland[^P5ch4_ap]. R n’est pas uniquement utilisé par des entreprises
logicielles comme Facebook ou Google, mais aussi par la Bank of America,
l’Agence américaine des produits alimentaires et médicamenteux et le
Service météorologique national américain, entre autres[^P5ch4_aq].

Quelques universités emploient également des programmeurs qui ont alors
la liberté de travailler à des projets *open source*. Par exemple, le
protocole d’heure réseau ou NTP (Network Time Protocol) utilisé pour
synchroniser le temps via Internet, fut d’abord développé par David
Mills, maintenant professeur émérite de l’université du Delaware —&nbsp;le
projet continuant à être maintenu par un groupe de volontaires conduit
par Harlan Stenn. Bash, l’outil de développement dont nous parlions dans
un chapitre précédent[^P5ch4_ar], est actuellement maintenu par Chet Ramsay, qui
est employé par le département des technologies de l’information de
l’université Case Western.

Les institutions universitaires ont le potentiel pour jouer un rôle
important dans le soutien de nouveaux projets, parce que cela coïncide
avec leurs missions et types de donation, mais elles peuvent aussi
manquer de la réactivité nécessaire pour attirer les nouveaux
programmeurs *open source*. NumFOCUS[^P5ch4_as] est un exemple d’une fondation
501(c)(3) qui soutient les logiciels scientifiques *open source* à
travers des donations et&nbsp; parrainages financiers.

Le modèle de la
fondation externe peut aider à fournir le soutien dont les logiciels
scientifiques ont besoin dans un contexte d’environnement universitaire.
Les fondations Alfred P. Sloan et Gordon & Betty Moore expérimentent
aussi des manières de connecter les institutions universitaires avec les
mainteneurs de logiciels d’analyse des données, dans le but de soutenir
un écosystème ouvert et durable[^P5ch4_at].

[^P5ch4_a]: [Données statistiques](https://w3techs.com/technologies/overview/web_server/all) sur W3techs.com, données en continu. En Janvier 2017, la part d'Apache est de 50,8%.
[^P5ch4_b]: Voir le site [Apache.org](http://www.apache.org/).
[^P5ch4_c]: Voir «&nbsp;[Apache Software Foundation](https://fr.wikipedia.org/wiki/Apache_Software_Foundation)&nbsp;», sur Wikipédia.
[^P5ch4_d]: Voir [la page Projets](https://sfconservancy.org/projects/current/) sur le site de la Software Freedom Conserancy (sfconservancy.org).
[^P5ch4_e]: voir Partie 3, Chapitre 2.
[^P5ch4_f]: Pour en savoir plus, voir sur le statut 501(c) des USA. Article «&nbsp;[501c](https://fr.wikipedia.org/wiki/501c)&nbsp;» sur Wikipédia. Le statut 501(c3) est assez proche du statut français des associations à but non lucratif déclarées d'utilité publique.
[^P5ch4_g]: Voir l'article «&nbsp;[IRS targeting controversy](https://en.wikipedia.org/wiki/IRS_targeting_controversy)&nbsp;» sur Wikipédia.
[^P5ch4_h]: Kelly Phillips Erb, «&nbsp;[Not Just The Tea Party: IRS Targeted and Turned Down Tax Exempt Status Tied To Open Source Software](http://www.forbes.com/sites/kellyphillipserb/2014/07/17/not-just-the-tea-party-irs-targeted-turned-down-tax-exempt-status-tied-to-open-source-software/)&nbsp;», *Forbes*, 17/07/2014.
[^P5ch4_i]: Jim Nelson, «&nbsp,[The new 501(c)(3) and the future of free software in the United States](https://blogs.gnome.org/jnelson/2014/06/30/the-new-501c3-and-the-future-of-free-software-in-the-united-states/)&nbsp;», *Jim Nelson and Yorba Foundation Archives* (blog), 30/06/2014.
[^P5ch4_j]: Idem.
[^P5ch4_k]: Présentation de la [Python Software Foundation](https://www.python.org/psf/), sur le site Python.org.
[^P5ch4_l]: Voir l'article «&nbsp;[501c](https://fr.wikipedia.org/wiki/501c)&nbsp;» sur Wikipédia.
[^P5ch4_m]: Voir le site [jquery.org/](https://jquery.org/).
[^P5ch4_n]: Django Software Foundation, «&nbsp;[2013 Annual Report](https://www.djangoproject.com/foundation/reports/2013/)&nbsp;», données de 2013.
[^P5ch4_o]: Andrew Godwin, «&nbsp;[Django awarded MOSS Grant](https://www.djangoproject.com/weblog/2015/dec/11/django-awarded-moss-grant/)&nbsp;», *DjangoProject.com*, 11/12/2015.
[^P5ch4_p]: [Source](http://collabprojects.linuxfoundation.org/sites/collabprojects/files/lf_collaborative_projects_brochure.pdf)sur *collabprojects.linuxfoundation.org* (01/03/2017 : lien cassé et renvoi sur la page d'accueil des projets de la fondation Linux).
[^P5ch4_q]: Paul Krill, «&nbsp;[Joyent staves off talk of a Node.js fork](http://www.infoworld.com/article/2835159/node-js/node-js-governance-model-pushed-as-forking-talk-ensues.html)&nbsp;», *InfoWorld*, 17/10/2014.
[^P5ch4_r]: Scott Hammond, «&nbsp;[Introducing the Node.js Foundation](https://www.joyent.com/blog/introducing-the-nodejs-foundation)&nbsp;», *Joyent*, 10/02/2015.
[^P5ch4_s]: Voir le site [Rubytogether.org](https://rubytogether.org/).
[^P5ch4_t]: Simon Phipps, «&nbsp;[Walmart's investment in open source isn't cheap](http://www.infoworld.com/article/2608897/open-source-software/walmart-s-investment-in-open-source-isn-t-cheap.html)&nbsp;», *InfoWorld*, 22/08/2014.
[^P5ch4_u]: Eran Hammer, «&nbsp;[Open Source ain’t Charity](https://hueniverse.com/2014/08/15/open-source-aint-charity/)&nbsp;», *Hueniverse*, 15/08/2014.
[^P5ch4_v]: Dion Almaer, «&nbsp;[Why we run an open source program - Walmart Labs](http://todogroup.org/blog/why-we-run-an-open-source-program-walmart-labs/)&nbsp;», *ToDo* (todogroup.org), 24/02/2015.
[^P5ch4_w]: Issue de tomchristie, «&nbsp;[What level to pitch pricing tiers?](https://github.com/pybee/paying-the-piper/issues/50)&nbsp;», sur *Paying the Piper* (projet Github), 28/10/2015.
[^P5ch4_x]: Voir Partie 3, Chapitre 3.
[^P5ch4_y]: Voir [page Gsoc](https://developers.google.com/open-source/gsoc/), sur *Google Summer of Code*.
[^P5ch4_z]: Voir l'article «&nbsp;[Summer of Code](https://fr.wikipedia.org/wiki/Google_Summer_of_Code)&nbsp;» sur Wikipédia.
[^P5ch4_aa]: Greg Brockman, «&nbsp;[Stripe Open-Source Retreat](https://stripe.com/blog/stripe-open-source-retreat)&nbsp;», *Stripe.com*, 24/04/2014.
[^P5ch4_ab]: Voir Partie 3, Chapitre 3.
[^P5ch4_ac]: Andrey Petrov, «&nbsp;[Urllib3, Stripe, and Open Source Grants](https://medium.com/@shazow/urllib3-stripe-and-open-source-grants-edb9c0e46e82#.632h98d3b)&nbsp;», *Medium.com*, 07/07/2014.
[^P5ch4_ad]: Écouter le [podcast de SystemsLive](https://s3.amazonaws.com/SystemsLive/Episode51.mp3), sur *systemslive.org*, épisode 51.
[^P5ch4_ae]: Bruce Schneier, «&nbsp;[When it comes to security, we're back to feudalism](https://www.wired.com/2012/11/feudal-security)&nbsp;», *Wired*, 26/11/2012.
[^P5ch4_af]: Seth Rosenblatt, «&nbsp;[Tech titans join forces to stop the next Heartbleed](https://www.cnet.com/news/tech-titans-join-forces-to-stop-the-next-heartbleed/)&nbsp;», *Cnet*, 24/04/2014.
[^P5ch4_ag]: Jim Zemlin, [The Linux Foundation’s Core Infrastructure Initiative Working with White House on Cybersecurity National Action Plan](https://www.linux.com/blog/linux-foundations-core-infrastructure-initiative-working-white-house-cybersecurity-national), *Linux.com*, 09/02/2016.
[^P5ch4_ah]: Mitchell Baker, «&nbsp;[Mozilla Launches Open Source Support Program](https://blog.mozilla.org/blog/2015/10/23/mozilla-launches-open-source-support-program/)&nbsp;», *The Mozilla Blog*, 23/10/2015.
[^P5ch4_ai]: Page [MOSS/Secure Open Source](https://wiki.mozilla.org/MOSS/Secure_Open_Source), sur *Mozilla Wiki*.
[^P5ch4_aj]: The Python Software Foundation, *[PSF Board Resolutions](https://www.python.org/psf/records/board/resolutions/)*, sur *python.org*.
[^P5ch4_ak]: Josh Constine, Gregory Ferenstein, «&nbsp;[Facebook Doesn’t Want To Be Cool, It Wants To Be Electricity](https://techcrunch.com/2013/09/18/facebook-doesnt-want-to-be-cool/)&nbsp;», *Techcrunch*, 18/09/2013.
[^P5ch4_al]: *[Dear Github](https://github.com/dear-github/dear-github)*, projet Github, 12/02/2016.
[^P5ch4_am]: Chris Dannen, «&nbsp;[Inside GitHub’s Super-Lean Management Strategy–And How It Drives Innovation](https://www.fastcompany.com/3020181/open-company/inside-githubs-super-lean-management-strategy-and-how-it-drives-innovation)&nbsp;», *Fastcompany*, 18/10/2013.
[^P5ch4_an]: Matt Weinberger, «&nbsp;[GitHub is undergoing a full-blown overhaul as execs and employees depart — and we have the full inside story](http://uk.businessinsider.com/github-the-full-inside-story-2016-2?r=US&IR=T)&nbsp;», *Business Insider*, 06/02/2016.
[^P5ch4_ao]: Heather Clancy, «&nbsp;[Slack Starts $80 Million App Investment Fund](http://fortune.com/2015/12/15/slack-app-investment-fund/)&nbsp;», *Fortune*, 18/12/2015.
[^P5ch4_ap]: R project, page *[Contributors](https://www.r-project.org/contributors.html)*, sur  *r-project.org*.
[^P5ch4_aq]: Voir *[Companies using R in 2014](http://www.revolutionanalytics.com/companies-using-r)*, sur revolutionanalytics.com.
[^P5ch4_ar]: Voir Partie 4, Chapitre 3.
[^P5ch4_as]: Voir la page *[Projects](http://www.numfocus.org/open-source-projects.html)*, Numfocus.org.
[^P5ch4_at]: Voir section «&nbsp;[Tools and Software](http://msdse.org/themes/#tools)&nbsp;», sur *msdse.org*.


