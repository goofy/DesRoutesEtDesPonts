## À la croisée des chemins

L’état actuel de notre infrastructure numérique est un des problèmes les
moins bien compris de notre temps. Il est vital de le comprendre.

En s’investissant bénévolement dans notre structure sous-jacente, les
développeurs ont facilité la construction de logiciels pour autrui. En
la fournissant gratuitement plutôt qu’en la facturant, ils ont alimenté
une révolution de l’information.

Les développeurs n’ont pas fait cela pour être altruistes. Ils l’ont
fait car c’était la meilleure manière de résoudre leurs propres
problèmes. L’histoire du logiciel *open source* est l’un des grands
triomphes de nos jours pour le bien public.

Nous avons de la chance que les développeurs aient limité les coûts
cachés de ces investissements. Mais leurs investissements initiaux ne
nous ont amenés que là où nous sommes aujourd’hui.

Nous ne sommes qu’au commencement de l’histoire de la transformation de
l’humanité par le logiciel. Marc Andreessen, co-fondateur de Netscape et
reconnu comme capital-risqueur derrière la société Andreessen Horowitz,
remarque[^P6ch3_a] en 2011 que «&nbsp;le logiciel dévore le monde&nbsp;».
Depuis lors, cette pensée est devenue un mantra pour l’ère moderne.

Le logiciel touche tout ce que l’on fait&nbsp;: non seulement les
frivolités et les loisirs, mais aussi les choses obligatoires et
critiques. OpenSSL, le projet décrit au début de cet essai[^P6ch3_b], le démontre
bien. Dans une interview téléphonique, Steve Marquess explique
qu’OpenSSL n’était pas utilisé seulement par les utilisateurs de sites
web, mais aussi par les gouvernements, les drones, les satellites et
tous *«&nbsp;les gadgets que vous entendez bipper dans les hôpitaux&nbsp;».

Le Network Time Protocol (protocole de temps par le réseau, NdT),
maintenu par Harlan Stenn, synchronise les horloges utilisées par des
milliards de périphériques connectés et touche tout ce qui contient un
horodatage. Pas seulement les applications de conversations ou les
courriels, mais aussi les marchés financiers, les enregistrements
médicaux et la production de produits chimiques.

Et pourtant, Harlan note[^P6ch3_c]&nbsp;:

> Il y a un besoin de soutenir l’infrastructure publique libre. Mais il
> n’y a pas de source de revenu disponible à l’heure actuelle. Les gens
> se plaignent lorsque leurs horloges sont décalées d’une seconde. Ils
> disent, «&nbsp;oui nous avons besoin de vous, mais nous ne pouvons pas
> vous donner de l’argent&nbsp;».

Durant ces cinq dernières années, l’infrastructure *open source* est
devenue une couche essentielle de notre tissu social. Mais tout comme
les *startups* ou la technologie elle-même, ce qui a fonctionné pour les
30 premières années de l’histoire de l’*open source* n’aidera plus à
avancer. Pour maintenir notre rythme de progression, nous devons
réinvestir dans les outils qui nous aident à construire des projets plus
importants et de meilleure qualité.

Trouver un moyen de soutenir l’infrastructure numérique peut sembler
intimidant, mais il y a de multiples raisons de voir le chemin à
parcourir comme une opportunité.

Premièrement, l’infrastructure est déjà là, avec une valeur clairement
démontrée. Ce rapport ne propose pas d’investir dans une idée sans
plus-value. L’énorme contribution sociale de l’infrastructure numérique
actuelle ne peut être ignorée ni mise de côté, comme cela est déjà
arrivé dans des débats tout aussi importants sur les données, la vie
privée, la neutralité du net, ou l’opposition entre investissement privé
et investissement public. Il est dès lors plus facile de faire basculer
les débats vers les solutions.

Deuxièmement, il existe déjà des communautés *open source* engagées et
prospères avec lesquelles travailler. De nombreux développeurs
s’identifient par le langage de programmation qu’ils utilisent (tels que
Python ou JavaScript), la fonction qu’ils apportent (telles qu’analyste
ou développeur opérationnels), ou un projet important (tels que Node.js
ou Rails). Ce sont des communautés fortes, visibles, et enthousiastes.

Les constructeurs de notre infrastructure numérique sont connectés les
uns aux autres, attentifs à leurs besoins, et techniquement talentueux.
Ils ont déjà construit notre ville&nbsp;; nous avons seulement besoin
d’aider à maintenir les lumières allumées, de telle sorte qu’ils
puissent continuer à faire ce qu’ils font de mieux.

Les infrastructures, qu’elles soient physiques ou numériques, ne sont
pas faciles à comprendre, et leurs effets ne sont pas toujours visibles,
mais cela doit nous encourager à les suivre plus en détail, pas moins.
Quand une communauté a parlé si ouvertement et si souvent de ses
besoins, tout ce que nous devons faire est d’écouter.

[^P6ch3_a]: Voir Marc Andreessen, «&nbsp;[Why Software Is Eating The World](http://www.wsj.com/articles/SB10001424053111903480904576512250915629460)&nbsp;», *The Wall Street Journal*, 20/08/2011.
[^P6ch3_b]: Voir Partie 1, Chapitre 2
[^P6ch3_c]: Voir Charles Babcock, «&nbsp;[NTP's Fate Hinges On 'Father Time'](http://www.informationweek.com/it-life/ntps-fate-hinges-on-father-time/d/d-id/1319432)&nbsp;», *InformationWeek*, 03/11/2015.


