## Avant-propos

Le problème exposé dans cet ouvrage m’est apparu sur une intuition. Pour
avoir travaillé dans des *startups* puis dans des sociétés de
capital-risque, j’ai pu constater que des sommes d’argent considérables
affluaient dans les entreprises de logiciel. Par ailleurs, en tant que
développeuse de logiciel en amateur, j’étais bien consciente que je
n’aurais rien pu produire toute seule. J’utilisais du code gratuit et
public (plus connu sous le nom de *code open source*) dont
j’assemblais des éléments afin de répondre à des objectifs personnels ou
commerciaux. Et franchement, les personnes impliquées dans ces projets
avaient, quel que soit leur rôle, fait le plus gros du travail.

Cette observation m’a tourné dans la tête pendant plusieurs années,
tandis que j’assistais à l’explosion à droite et à gauche des
*bootcamps*[^P1ch1_a] où étaient diplômés de nouveaux développeurs de logiciel et
que je voyais des *startups* lever plusieurs dizaines de millions de
dollars pour vendre des produits qui tournaient sans doute avec plus de
code libre que de code propriétaire. Ayant précédemment travaillé dans
des associations à but non lucratif, je faisais immédiatement le lien
avec les biens publics et les défis qui leur sont associés. Pourtant ce
vocabulaire était étrangement absent du langage de mes pairs dans le
monde du logiciel.

Après avoir quitté mon travail dans une entreprise de capital-risque
l’an dernier, je me suis mis en tête d’étudier ce paradoxe auquel je ne
cessais de penser&nbsp;: il existe des logiciels précieux qui ne peuvent pas
s’appuyer sur des modèles commerciaux et auxquels manquent le soutien
des pouvoirs publics.

C’est plutôt amusant, mais le code *open source* ne figurait pas sur ma
liste initiale. Comme mes collègues, j’avais supposé, à tort, que
c’était l’exemple même de ressources logicielles à la disposition du
public qui bénéficiaient d’un fort soutien. Lorsque j’ai mentionné
l’*open source* à mes amis et mentors, ils m’ont aimablement dissuadée
de poursuivre mes recherches dans ce domaine, puis incitée à plutôt
trouver d’autres exemples de domaines qui avaient vraiment besoin de
soutien.

Pourtant, je suis tombée sur un certain nombre de projets *open source*
qui mettaient à mal ces préjugés. Il s’est avéré que maintenir les
projets dans la durée était un problème connu dans le monde des
contributeurs de l’*open source*. Plus je creusais la question et plus
je découvrais des billets de blog, des articles et des forums de
discussion qui abordaient la tension et l’épuisement éprouvés par ceux
qui maintiennent les projets *open source*. Tout le monde m’indiquait
une autre personne à contacter et sans m’en apercevoir j’ai récolté un
nombre incroyable de témoignages à ce sujet.

Je me suis rendu compte que j’avais découvert un problème certes «&nbsp;bien
connu&nbsp;» des producteurs (les contributeurs de l’*open source*) mais dont
les consommateurs (les entreprises de logiciels et les autres
utilisateurs de code *open source*) n’avaient apparemment aucune idée.
Cette anomalie m’a incitée à me pencher sur le problème.

Par ailleurs, il semble que le milieu de l’*open source* soit lui-même
en train d’évoluer, voire de bifurquer. J’ai eu des conversations très
diverses avec des interlocuteurs de différentes générations, tous
contributeurs *open source*. Ils semblaient avoir des philosophies et
des valeurs divergentes, au point de donner l’impression de ne pas
utiliser le même vocabulaire. J’ai appris que dans les trois à cinq
dernières années, la production ainsi que la demande avaient explosé
dans le monde de l’*open source* grâce à l’amélioration des outils pour
les développeurs et à celle de l’organisation du travail. Les
contributeurs de l’*open source* d’aujourd’hui sont très différents de
ceux d’il y a 10 ans, sans parler de ceux d’il y a 30 ans. Or ces
différentes générations ne communiquent pas entre elles, ce qui rend
difficile toute conversation productive sur la maintenance pérenne des
logiciels.

> Au hasard d’une conversation avec Ethan Zuckerman, du MIT[^P1ch1_b] Center for Civic Media[^P1ch1_c], j’ai eu l’occasion de partager plus largement mes découvertes.

Bien que ne sachant pas exactement ce qu’il y avait derrière ni si
j’employais les bons mots, j’ai décrit à Ethan le problème dont je
m’étais rendu compte et il a eu la gentillesse de me mettre en contact
avec Jenny Toomey de la Fondation Ford. Jenny m’a suggéré de rassembler
les résultats de mes recherches dans un rapport. Au fur et à mesure de
son écriture a émergé cet ouvrage sur notre société numérique moderne,
et sur l’infrastructure cachée qui la sous-tend.

Le présent ouvrage n’aurait jamais vu le jour si Ethan et Jenny
n’avaient pas donné sa chance à une idée tout juste ébauchée qui
désormais, grâce au travail d’écriture, s’est transformée en quelque
chose de construit. Je les remercie énormément d’avoir fait confiance à
leur intuition. Je suis aussi reconnaissante envers Michael Brennan et
Lori McGlinchey pour leurs conseils, leur regard, et leur enthousiasme
au cours de la relecture. Enfin, et c’est sans doute le plus important,
j’ai une dette envers toutes les personnes qui travaillent dans l’*open
source* et qui ont rendu leur histoire publique pour que des gens comme
moi puissent la lire —&nbsp;et particulièrement ceux qui ont pris de leur
temps malgré un agenda chargé pour me divertir au détour d’une
conversation ou d’un courriel. Ce rapport est un concentré de leur
sagesse et non de la mienne. Je suis particulièrement reconnaissante
pour les conversations que j’ai pu avoir avec Russel Keith-Magee, Eric
Holscher, Jan Lehnardt, Audrey Petrov et Mikeal Rogers, ils continuent à
m’inspirer par leur patience et leur dévouement à l’égard du travail
*open source*.

Merci d’avoir été aussi attentionnés.

[^P1ch1_a]: Un *bootcamp* est une formation accélérée au métier de développeur informatique. À l'origine le terme désignait les camps d'entraînement des Marines américains.
[^P1ch1_b]: Le Massachusetts Institute of Technology (MIT), en français Institut de technologie du Massachusetts, est un institut de recherche et une université américaine, spécialisée dans les domaines de la science et de la technologie. Située à Cambridge, dans le Massachusetts, à proximité immédiate de Boston, au nord-est des États-Unis, elle est souvent considérée au <span style="font-variant: small-caps;">XXI</span><up>e</up> siècle comme une des meilleures universités mondiales en sciences et en technologie (source [Wikipédia](https://fr.wikipedia.org/wiki/Massachusetts_Institute_of_Technology)).
[^P1ch1_c]: Le *Center for Civic Media* est un laboratoire du MIT, «&nbsp;travaillant main dans la main avec différentes communautés pour créer, concevoir, déployer et évaluer collaborativement des outils et des pratiques de communication.&nbsp;» (Voir la page «&nbsp;À propos&nbsp;» sur [civic.mit.edu](https://civic.mit.edu/about)).




