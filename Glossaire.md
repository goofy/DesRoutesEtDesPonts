## Glossaire

Bibliothèque (logicielle)

: Les bibliothèques sont des fonctions préfabriquées qui accélèrent l’écriture du code d’un logiciel, tout comme une entreprise du bâtiment achète des fenêtres préfabriquées au lieu de les assembler à partir des composants de base. Par exemple, au lieu de développer leur propre système d’identification pour les utilisateurs de leur application, les développeurs et développeuses peuvent utiliser une bibliothèque appelée OAuth.

Capital-risque

: Un type d’investissement privé qui permet de financer de jeunes entreprises à forte croissance en échange de titres. Le capital risque a participé à la croissance de nombreux aspects commerciaux d’Internet et a permis la montée en puissance de la Silicon Valley.

Code source

: Pour les besoins de ce rapport, le code source correspond au code effectivement associé à un projet *open source*.

Contributeur, contributrice

: Personne qui fait une contribution à un *open source* actif. Parmi les exemples de contribution ont peut mentionner : écrire du code, rédiger de la documentation, s’occuper de l’assistance aux utilisateurs, etc. Un contributeur n’a pas forcément un droit de commit sur le projet (par exemple quelqu’un d’autre doit approuver ou non ses contributions avant de les mettre en production).

Dépôt (logiciel) 

: Lieu de stockage du code source nécessaire pour utiliser un projet informatique
Par exemple, GitHub offre un espace en ligne pour héberger les dépôts de chacun et permettre à d’autres autres utilisateurs de les trouver et de les utiliser.
Appelé aussi communément un «&nbsp;dépôt&nbsp;». L’ensemble des fichiers de code source est appelé une «&nbsp;base de code&nbsp;».

Documentation (de logiciel)

: Information écrite qui explique comment les gens peuvent utiliser ou contribuer à un projet logiciel. La documentation est comme un manuel d’instructions pour le logiciel. Sans elle, un développeur ne saurait pas comment utiliser au mieux le projet.


OSS

: Acronyme de *Open Source Software* logiciel *open source*  

FOSS

: Acronyme de *Free and Open Source Software* logiciel *open source* gratuit.

FLOSS

: Acronyme de *Free, Libre, and Open Source Software* logiciel *open source* libre et gratuit 

Fork (scission)

: Il existe deux types de *forks*. Historiquement, un *fork* de projet est une copie d’un projet *open source* que l’on continue de le développer séparément. Son usage est pratiquement politique par exemple, lorsqu’il y a un désaccord interne à propos de la direction du projet, ou pour imposer des modifications substantielles au projet d’origine (idéalement, en réintégrant en retour ces modifications au projet principal). Un *fork* sur GitHub fait référence à une copie temporaire d’un projet pour réaliser des modifications, généralement avec l’intention de les réintégrer ensuite dans le projet principal. Cela n’a pas la connotation politique et sociale d’un *fork* de projet. GitHub a remanié ce terme pour encourager une culture de bidouillage léger qui prévaut maintenant parmi les contributeurs *open source* d’aujourd’hui.

Framework

: Les *framewoks* offrent une base, une sorte d’échafaudage, une structure. Imaginez cela comme un schéma pour toute une application. Comme un plan, un *framework* définit la manière dont l’application se comportera sur mobile, ou comment ses données seront sauvegardées dans une base de données. Rails et Django sont des exemples de *frameworks*.  Selon Wikipédia, «&nbsp;*Les frameworks sont acquis par les informaticiens, puis incorporés dans des logiciels applicatifs mis sur le marché, ils sont par conséquent rarement achetés et installés séparément par un utilisateur final.*&nbsp;»

GitHub

: Plateforme commerciale d’hébergement de code. Github a été lancé en 2008 et c’est actuellement la plateforme la plus populaire pour héberger du code et collaborer à des projets *open source* (il est également possible de stocker du code privé sur Github). Github a aidé à standardiser les pratiques dans le développement *open source* et a élargi l’audience de l’*open source*. La gestion des projets sur Github se fait grâce au système de contrôle de version Git.

Infrastructure numérique

: Pour les besoins de cet ouvrage, l’infrastructure numérique signifie l’ensemble des ressources logicielles publiques qui sont utilisées pour créer des logiciels destinés à un usage personnel ou commercial. On peut prendre l’exemple des langages de programmation ou celui des bases de données. Cette définition n’inclut pas les éléments d’infrastructure physique nécessaires à la création de logiciel (p. ex. les serveurs physiques, les câbles…)

Langage (de programmation)

: Les langages de programmation sont l’ossature de communication des logiciels. Ils permettent aux différents composants logiciels de réaliser des actions et d’interagir entre eux. On peut citer parmi les langages les plus populaires le JavaScript, le Python et le C.

Logiciel libre

: Logiciel que l’on peut librement exécuter pour n’importe quel objectif (commercial ou non) aussi bien qu’étudier, modifier et distribuer. Le terme a été créé aux alentours de 1983, à partir des travaux de l’informaticien Richard Stallman, du projet GNU, et de la Free Software Foundation (fondée en 1985).

Mainteneur (*open source*) 

: Personne qui endosse la responsabilité d’un projet *open source*. La fonction varie suivant les projets. Parfois, les mainteneurs sont formellement nommés ; et parfois, ceux qui abattent le plus gros du travail le deviennent *de facto*. Un mainteneur porte probablement la charge de la gestion du projet plus qu’aucun autre contributeur. Il peut être ou non l’auteur de la version initiale du projet. Il y a de fortes chances qu’il ait un accès *commit* sur le projet, c’est-à-dire qu’il peut faire des modifications en direct sur le projet. 

Open source (logiciel)

: Techniquement, un logiciel *open source* est la même chose qu’un logiciel libre (voir ci-dessus). Néanmoins, d’un point de vue culturel, l’*open source* tend à souligner les bénéfices pratiques des logiciels publics, alors que le logiciel libre est davantage un mouvement social.  
Le terme *open source* a vu le jour en 1998, au cours d’une réunion entres personnes qui recherchaient une alternative au terme *Free Software* qui soit plus en phase avec l’entreprise.


