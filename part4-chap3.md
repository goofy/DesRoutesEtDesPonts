## Pourquoi ces projets sont-ils si difficiles à financer&nbsp;?

Aujourd’hui, le travail sur les infrastructures numériques est effectué
par des développeurs *freelance* ou ayant un «&nbsp;job alimentaire&nbsp;», leur
temps libre est consacré aux projets *open source* mais le reste du
temps ils font un travail rémunéré sans rapport avec ces projets. Même
si c’est un moyen réaliste pour financer son quotidien, cela ne permet
pas d’apprécier à sa juste valeur l’apport social de ces projets.

Étonnamment, bien que tout le monde soit d’accord pour reconnaître qu’il
y a un problème (qu’on le qualifie de «&nbsp;*burnout* du bénévole&nbsp;», de
mauvaise gestion de la communauté ou de manque de financement
suffisant), la discussion ne dépasse pas le stade de maigres solutions à
court-terme comme les «&nbsp;pourboires&nbsp;» ou le *crowdfunding*.

Discutez avec des développeurs qui ont trouvé un moyen de gagner leur
vie, et vous entendrez le mot «&nbsp;chanceux&nbsp;» à tout bout de champ&nbsp;:
chanceux d’avoir été embauché par une entreprise, chanceux d’avoir eu de
la notoriété et des dons, chanceux d’être tombé sur un modèle économique
viable, chanceux de ne pas avoir une famille ou un prêt dont
s’inquiéter. Tout le monde peut être chanceux. Mais la chance dure
quelques mois, peut-être un an ou deux, et puis elle s’épuise.

Pourquoi est-il si difficile de financer les infrastructures
numériques&nbsp;?

Fondamentalement, l’infrastructure numérique a un problème de passagers
clandestins. Les ressources sont disponibles gratuitement, et tout le
monde les utilise (qu’il s’agisse de développeurs individuels ou de
grandes entreprises de logiciels), mais personne n’est encouragé à
contribuer en retour, chacun s’imaginant qu’un autre finira par le
faire. S’il est laissé à l’abandon, ce problème mènera à une tragédie
des communs.

En plus de l’enjeu macroéconomique des communs, il y a plusieurs raisons
pour lesquelles le financement des infrastructures numériques est
particulièrement compliqué. Ces raisons ont déjà été abordées au cours
de cette étude, mais sont toutes résumées ici.

*On croit à tort qu’il s’agit d’un «&nbsp;problème résolu&nbsp;».*

Même parmi les acteurs du secteur comme les entreprises de logiciels, la
croyance est très répandue que l’*open source* est déjà correctement
financée, ce qui rend d’autant plus difficile la levée de fonds.
Certains projets d’infrastructure fonctionnent durablement, soit parce
qu’ils disposent d’un modèle économique viable ou de mécènes, soit parce
que les coûts de maintenance sont limités. Un public novice pourra
également faire le lien entre l’*open source* et des entreprises telles
que *Red Hat* ou *Docker* et penser que le problème a été résolu. Mais
il faut garder à l’esprit que ces cas sont l’exception et non la règle.

*Il manque une prise de conscience et une compréhension culturelle de
ce problème.*

En dehors de la communauté *open source*, tout le monde, ou presque,
ignore les problèmes de financement de ces projets d’infrastructure, et
le sujet est perçu comme plutôt aride et technique. Les développeurs qui
ont besoin de soutien ont tendance à se concentrer principalement sur la
technique et sont mal à l’aise lorsqu’il s’agit de défendre l’aspect
financier de leur travail. Au bout du compte, on ne parvient pas à
trouver l’élan qui pourrait modifier cette situation en panne.

*Les infrastructures numériques sont enracinées dans l’*open source*,
dont la culture du bénévolat n’encourage pas à parler d’argent.*

Même si cette attitude a fait de l’*open source* ce qu’elle est
aujourd’hui, elle crée également un tabou qui rend difficile pour les
développeurs l’évocation de leurs besoins, car ils se sentent coupables
ou ont peur de passer pour des personnes qui n’auraient pas l’esprit
d’équipe. La nature hautement décentralisée et démocratique de l’*open
source* rend également difficile la coordination et le financement
d’acteurs institutionnels qui pourraient défendre leurs intérêts.

*Les infrastructures numériques sont hautement décentralisées,
contrairement aux infrastructures physiques.*

Contrairement à un projet de construction de pont, il n’est pas toujours
évident de savoir quels projets seront utiles avant qu’ils n’aient déjà
décollé. Ils ne peuvent pas être planifiés à l’avance par un organisme
centralisé. Et à l’autre bout du cycle de vie, certains projets sont
destinés à tomber en désuétude à mesure que d’autres solutions,
meilleures, prendront leur place. L’infrastructure numérique est
constituée de centaines de projets, grands ou petits, réalisés par des
individus, des groupes ou des entreprises&nbsp;; en faire l’inventaire serait
un travail de titan.

> Il est difficile de trouver des financements… pour le développeur
> moyen (comme moi), certains sont totalement hors de portée.
> *Kickstarter* ne marche que si tu deviens viral, ou si tu embauches
> quelqu’un pour faire tout ce qui est marketing /&nbsp;design /&nbsp;promotion…
> Transformer un projet en entreprise c’est génial aussi mais… ce sont
> des choses qui t’éloignent du développement (qui est la partie qui
> m’intéresse). Si je voulais obtenir une subvention, je ne saurais même
> pas par où commencer.

– Kyle Kemp[^P5ch3_a], développeur *freelance* et contributeur *open source*.

[^P5ch3_a]: [Commentaire de seiyria](https://github.com/pybee/paying-the-piper/issues/53) sur *Paying the piper* (projet Github), 28/10/2015 (en anglais)


